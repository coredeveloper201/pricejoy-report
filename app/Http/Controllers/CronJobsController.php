<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\CronJobs;
use App\Product;
use App\ProductType;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class CronJobsController extends Controller
{
    public function __construct()
    {
        ini_set('max_execution_time', 300);
    }

    public function index()
    {
        $jobs = CronJobs::paginate(10);
        return view('cron-jobs', compact('jobs'));
    }

    public function uploadCSV(Request $request)
    {
        $success = false;
        $message = '';
        $file = $request->file('csv_file');
        $destinationPath = 'temp';
        $file_name = $file->getClientOriginalName();
        $file->move($destinationPath, $file_name);
        try {
            $file_pointer = fopen(public_path('temp/' . $file_name), "r");
            $data = [];
            while ($line = fgetcsv($file_pointer, 0, ",")) {
                $data[] = $line;
            }
            fclose($file_pointer);
            $x = [];
            foreach ($data as $row) {
                $check = explode('.', $row[0]);
                if ($check[1] == 'csv') {
                    $x['title'] = $row[0];
                    $x['url'] = 'https://s3.amazonaws.com/sando-awin-files/' . $row[0];
                    $insert = CronJobs::create($x);
                    if ($insert) {
                        $message = 'Data inserted successfully';
                        $success = true;
                    } else {
                        $success = false;
                        $message = 'Whoops! failed to insert Data';
                        break;
                    }
                }
            }
            return response()->json(['success' => $success, 'message' => $message]);
        } catch (\Exception $e) {
            $message = [
                'info' => 'Whoops! Failed to upload file.',
                'exception' => $e->getMessage(),
            ];
            return response()->json(['success' => false, 'message' => $message]);
        }
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required',
            'url' => 'required',
        ]);
        $insert = CronJobs::create($validatedData);
        if ($insert) {
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function updateStatus(Request $request)
    {
        $validatedData = $request->validate([
            'id' => 'required',
        ]);
        $job = CronJobs::findOrFail($validatedData['id']);
        if ($job->status == 0) {
            $status = 1;
        } else {
            $status = 0;
        }
        $update = $job->update(['status' => $status]);
        if ($update) {
            return redirect()->back();
        } else {
            return redirect()->back();
        }
    }

    public function parseData()
    {
        $job = CronJobs::where('status', 1)->first();
        if ($job) {
            try {
                $url = $job->url;
                $contents = file_get_contents($url);
                $file = file_put_contents(public_path('/temp/' . $job->title), $contents);

                if ($file) {
                    $filename = $job->title;
                    $file_pointer = fopen(public_path('temp/' . $filename), "r");
                    $data = [];
                    while ($line = fgetcsv($file_pointer, 0, ",")) {
                        $data[] = $line;
                    }
                    fclose($file_pointer);
                    return $this->insertProduct($data);
                }
            } catch (\Exception $e) {
                $message = [
                    'info' => 'Whoops! Failed to upload file.',
                    'exception' => $e->getMessage(),
                ];
                return response()->json(['success' => false, 'message' => $message]);
            }
        } else {
            return response()->json(['success' => false, 'message' => 'Sorry! There is no active JOBs']);
        }
    }

    public function insertProduct($data)
    {
        $time_start = microtime(true);
        $success = false;
        $row_count = 0;
        foreach ($data as $key => $product) {
            if ($key > 0) {
                $row_count++;
                $col = -1;
                $inserted_product = Product::create([
                    'aw_deep_link' => $product[$col += 1],
                    'product_name' => $product[$col += 1],
                    'aw_product_id' => $product[$col += 1],
                    'merchant_product_id' => $product[$col += 1],
                    'merchant_image_url' => $product[$col += 1],
                    'description' => $product[$col += 1],
                    'merchant_category' => $product[$col += 1],
                    'search_price' => $product[$col += 1],
                    'merchant_name' => $product[$col += 1],
                    'merchant_id' => $product[$col += 1],
                    'category_name' => $product[$col += 1],
                    'category_id' => $product[$col += 1],
                    'aw_image_url' => $product[$col += 1],
                    'currency' => $product[$col += 1],
                    'store_price' => $product[$col += 1],
                    'delivery_cost' => $product[$col += 1],
                    'merchant_deep_link' => $product[$col += 1],
                    'language' => $product[$col += 1],
                    'last_updated' => $product[$col += 1],
                    'display_price' => $product[$col += 1],
                    'data_feed_id' => $product[$col += 1],
                    'brand_name' => $product[$col += 1],
                    'brand_id' => $product[$col += 1],
                    'colour' => $product[$col += 1],
                    'product_short_description' => $product[$col += 1],
                    'specifications' => $product[$col += 1],
                    'condition' => $product[$col += 1],
                    'product_model' => $product[$col += 1],
                    'model_number' => $product[$col += 1],
                    'dimensions' => $product[$col += 1],
                    'keywords' => $product[$col += 1],
                    'promotional_text' => $product[$col += 1],
                    'product_type' => $product[$col += 1],
                    'commission_group' => $product[$col += 1],
                    'merchant_product_category_path' => $product[$col += 1],
                    'merchant_product_second_category' => $product[$col += 1],
                    'merchant_product_third_category' => $product[$col += 1],
                    'rrp_price' => $product[$col += 1],
                    'saving' => $product[$col += 1],
                    'savings_percent' => $product[$col += 1],
                    'base_price' => $product[$col += 1],
                    'base_price_amount' => $product[$col += 1],
                    'base_price_text' => $product[$col += 1],
                    'product_price_old' => $product[$col += 1],
                    'delivery_restrictions' => $product[$col += 1],
                    'delivery_weight' => $product[$col += 1],
                    'warranty' => $product[$col += 1],
                    'terms_of_contract' => $product[$col += 1],
                    'delivery_time' => $product[$col += 1],
                    'in_stock' => $product[$col += 1],
                    'stock_quantity' => $product[$col += 1],
                    'valid_from' => $product[$col += 1],
                    'valid_to' => $product[$col += 1],
                    'is_for_sale' => $product[$col += 1],
                    'web_offer' => $product[$col += 1],
                    'pre_order' => $product[$col += 1],
                    'stock_status' => $product[$col += 1],
                    'size_stock_status' => $product[$col += 1],
                    'size_stock_amount' => $product[$col += 1],
                    'merchant_thumb_url' => $product[$col += 1],
                    'large_image' => $product[$col += 1],
                    'alternate_image' => $product[$col += 1],
                    'aw_thumb_url' => $product[$col += 1],
                    'alternate_image_two' => $product[$col += 1],
                    'alternate_image_three' => $product[$col += 1],
                    'alternate_image_four' => $product[$col += 1],
                    'reviews' => $product[$col += 1],
                    'average_rating' => $product[$col += 1],
                    'rating' => $product[$col += 1],
                    'number_available' => $product[$col += 1],
                    'custom_1' => $product[$col += 1],
                    'custom_2' => $product[$col += 1],
                    'custom_3' => $product[$col += 1],
                    'custom_4' => $product[$col += 1],
                    'custom_5' => $product[$col += 1],
                    'custom_6' => $product[$col += 1],
                    'custom_7' => $product[$col += 1],
                    'custom_8' => $product[$col += 1],
                    'custom_9' => $product[$col += 1],
                    'ean' => $product[$col += 1],
                    'isbn' => $product[$col += 1],
                    'upc' => $product[$col += 1],
                    'mpn' => $product[$col += 1],
                    'parent_product_id' => $product[$col += 1],
                    'product_GTIN' => $product[$col += 1],
                    'basket_link' => $product[$col += 1],
                ]);
                if ($inserted_product) {
//                    return $inserted_product; // For Debugging purpose
                    $success = true;
                } else {
                    $success = false;
                }
            }
        }
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);
        if ($success) {
            $message = 'Data Successfully inserted';
        } else {
            $message = 'Whoops! Data insertion failed!';
        }
        return response()->json(['success' => $success, 'no_of_rows_in_the_file' => $row_count, 'message' => $message, 'execution_time' => $execution_time . ' Seconds']);
    }

}
