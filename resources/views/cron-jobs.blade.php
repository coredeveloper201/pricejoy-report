@extends('app')

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('upload-csv') }}" method="post" enctype="multipart/form-data">
                @csrf
                <p>Upload CSV</p>
                <div class="custom-file mb-3">
                    <input type="file" class="custom-file-input" id="csv_file" name="csv_file">
                    <label class="custom-file-label" for="csv_file">Choose CSV</label>
                </div>
                <div class="mt-3">
                    <button type="submit" class="btn btn-success">Upload</button>
                </div>
            </form>
        </div>
    </div>

    <div class="mb-10">
        <hr/>
        <hr/>
    </div>

    <div class="card">
        <div class="card-body">
            <form method="post" action="{{url()->current()}}">
                @csrf
                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" class="form-control" id="title" name="title" placeholder="Enter a Title">
                </div>
                <div class="form-group">
                    <label for="url">Url</label>
                    <input type="text" class="form-control" id="url" name="url" placeholder="Enter Link">
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>

    <div class="mt-5">
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Title</th>
                <th scope="col">URL</th>
                <th scope="col">Status</th>
            </tr>
            </thead>
            <tbody>
            @foreach($jobs as $key => $job)
                <form action="{{ route('change-status') }}" method="post">
                    @csrf
                    <input type="hidden" name="id" value="{{ $job->id }}">
                    <tr data-id="{{ $job->id }}">
                        <th scope="row">{{ $job->title }}</th>
                        <td>{{ $job->url }}</td>
                        <td>
                            @if ($job->status == 0)
                                <button class="btn btn-primary">Inactive</button>
                            @else
                                <button class="btn btn-success">Active</button>
                            @endif
                        </td>
                    </tr>
                </form>
            @endforeach
            </tbody>
        </table>
        <div class="content-justify-center">
            {{ $jobs->links() }}
        </div>
    </div>
@stop
