<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('aw_deep_link')->nullable();
            $table->string('product_name')->nullable();
            $table->string('aw_product_id')->nullable();
            $table->string('merchant_product_id')->nullable();
            $table->string('merchant_image_url')->nullable();
            $table->text('description')->nullable();
            $table->string('merchant_category')->nullable();
            $table->string('search_price')->nullable();
            $table->string('merchant_name')->nullable();
            $table->string('merchant_id')->nullable();
            $table->string('category_name')->nullable();
            $table->string('category_id')->nullable();
            $table->text('aw_image_url')->nullable();
            $table->string('currency')->nullable();
            $table->string('store_price')->nullable();
            $table->string('delivery_cost')->nullable();
            $table->text('merchant_deep_link')->nullable();
            $table->string('language')->nullable();
            $table->string('last_updated')->nullable();
            $table->string('display_price')->nullable();
            $table->string('data_feed_id')->nullable();
            $table->string('brand_name')->nullable();
            $table->string('brand_id')->nullable();
            $table->string('colour')->nullable();
            $table->text('product_short_description')->nullable();
            $table->string('specifications')->nullable();
            $table->string('condition')->nullable();
            $table->string('product_model')->nullable();
            $table->string('model_number')->nullable();
            $table->string('dimensions')->nullable();
            $table->string('keywords')->nullable();
            $table->string('promotional_text')->nullable();
            $table->string('product_type')->nullable();
            $table->string('commission_group')->nullable();
            $table->string('merchant_product_category_path')->nullable();
            $table->string('merchant_product_second_category')->nullable();
            $table->string('merchant_product_third_category')->nullable();
            $table->string('rrp_price')->nullable();
            $table->string('saving')->nullable();
            $table->string('savings_percent')->nullable();
            $table->string('base_price')->nullable();
            $table->string('base_price_amount')->nullable();
            $table->string('base_price_text')->nullable();
            $table->string('product_price_old')->nullable();
            $table->string('delivery_restrictions')->nullable();
            $table->string('delivery_weight')->nullable();
            $table->string('warranty')->nullable();
            $table->text('terms_of_contract')->nullable();
            $table->string('delivery_time')->nullable();
            $table->string('in_stock')->nullable();
            $table->string('stock_quantity')->nullable();
            $table->string('valid_from')->nullable();
            $table->string('valid_to')->nullable();
            $table->string('is_for_sale')->nullable();
            $table->string('web_offer')->nullable();
            $table->string('pre_order')->nullable();
            $table->string('stock_status')->nullable();
            $table->string('size_stock_status')->nullable();
            $table->string('size_stock_amount')->nullable();
            $table->string('merchant_thumb_url')->nullable();
            $table->string('large_image')->nullable();
            $table->string('alternate_image')->nullable();
            $table->text('aw_thumb_url')->nullable();
            $table->text('alternate_image_two')->nullable();
            $table->text('alternate_image_three')->nullable();
            $table->text('alternate_image_four')->nullable();
            $table->text('reviews')->nullable();
            $table->text('average_rating')->nullable();
            $table->text('rating')->nullable();
            $table->text('number_available')->nullable();
            $table->text('custom_1')->nullable();
            $table->text('custom_2')->nullable();
            $table->text('custom_3')->nullable();
            $table->text('custom_4')->nullable();
            $table->text('custom_5')->nullable();
            $table->text('custom_6')->nullable();
            $table->text('custom_7')->nullable();
            $table->text('custom_8')->nullable();
            $table->text('custom_9')->nullable();
            $table->string('ean')->nullable();
            $table->string('isbn')->nullable();
            $table->string('upc')->nullable();
            $table->string('mpn')->nullable();
            $table->string('parent_product_id')->nullable();
            $table->text('product_GTIN')->nullable();
            $table->text('basket_link')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
